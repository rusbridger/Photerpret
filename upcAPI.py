from semantics3 import Products
import json

def upcAPI(upc):
    sem3 = Products(
        api_key="SEM37CDFD7F8EBACB182ECF21725E372C58E",
        api_secret="YmI5YjU0ZDk0NjE1Y2I5ZGQ3NWRmNTc2ZmY5YjQ4ZGM"
    )

    sem3.products_field("upc", upc)

    results = sem3.get_products().get("results")[0]
    upc_import = ["category","features","manufacturer","name","Container Type",
                  "features","recommended use","dimensions","capacity","food allergen statements",
                  "blob","food form","brand","height","length","width","size","weight"]
    upc_info = {}
    for key in upc_import:
        if (results.get(key) != None):
            upc_info[key] = results.get(key)

    upc_dict = {}

    for key in upc_info:
        if (key != "features"):
            upc_dict[key] = upc_info.get(key)
        else:
            for descKey in upc_info.get("features"):
                upc_dict[descKey] = upc_info.get("features").get(descKey)

    #upc_output = {"UPC Product Info: ":upc_dict}
    
    #print(upc_output)
    return(upc_dict)

